Software installation in Ubuntu, Lubuntu and Mint
=================================================

.. important::

    * First, open "Software updates" and select "main repository" and  enable "source code".
    * Then update the cache when prompted or run below command
      
        .. code-block:: shell
        
            sudo apt-get update  

    * I tried various other distros as well e.g. CentOS, Manjaro and OpenSUSE etc. But I like "Lubuntu" as it is lightweight and the software (related to Electronics designs) are very easy to install as compare to other distros. Also, use below method to create shortcuts in Lubuntu, 
    
        .. code-block:: shell
        
            (open lubuntu-rc.xml)
            sudo gvim ~/.config/openbox/lubuntu-rc.xml

            (define C-A-End, i.e. ctrl+alt+end, to open logout screen)
            (add below line between <keyboard> and </keyboard>)

            <keybind key="C-A-End">
            <action name="Execute">
            <command>lubuntu-logout</command>
            </action>
            </keybind>

    
.. note::

    Next install the 'apt-fast' as it allows fast downloading as shown next. Then update and upgrade the operating system.


apt-fast
--------

    * Used for high speed software-updates using "apt-fast" (instead of "apt-get")  
    * During installation process, select "apt-get" and "max connection = 10"
        
        .. code-block:: shell

            sudo add-apt-repository ppa:saiarcot895/myppa && sudo apt-get update && sudo apt-get install apt-fast

    * Then update and upgrade the Linux

        .. code-block:: shell

            sudo apt-fast update && sudo apt-fast upgrade


Remove username from terminal
-----------------------------

Modify the .bashrc file as below, to remove the username and working directory etc. from the terminal,

    .. code:: shell
        
        $ cd 

        (use "xed" in Mint and "gedit" in Ubuntu in below command)
        $ gedit .bashrc

        (add anyone of the following lines at the end)

        # display $ (or # for root)
        export PS1="\$ "
        
        # display user-name $
        export PS1="\u \$ "

        # display user-name@working-dir $
        export PS1="\u@\w \$ "


Partition manager
-----------------

    .. code-block:: shell
    
        sudo apt-fast install gparted


Terminator
----------
    
    * Advance terminal that allows split-screen and other features     
     
        .. code-block:: shell

            sudo apt-fast install terminator

Tree
----

    * 'tree' command can be useful see the directory structure, 
    
        .. code-block:: shell
        
            sudo apt-fast install tree

TMUX
----

    * TMUX can be used with any of the terminal to enhance the functionalities e.g. splitting windows etc. Some of the keyboard shortcuts are added in `TMUX Guide <http://tmuxguide.readthedocs.io/en/latest/>`_
      
    .. code-block:: shell
    
        sudo apt-fast install tmux

Git
---

    A list of Git-commands is shown at `Git Guide <http://gitguide.readthedocs.io/en/latest/gitguide/index.html>`_

    .. code-block:: shell

        sudo apt-fast install git


VIM
---

    * A list of Vim-commands is shown at `Vim Guide <http://vimguide.readthedocs.io/en/latest/index.html>`_

    .. code-block:: shell

        sudo add-apt-repository ppa:jonathonf/vim && sudo apt-fast update && sudo apt-fast install vim-gtk


    *  Download :download:`.vimrc <download/vimrc.txt>` file and **save to home folder (rename it to `.vimrc')**. And comment/uncomment/add plugins as per requirement,
        
    * Install Vundle : This is used for managing plugins
    
        .. code-block:: shell
        
            git clone --depth 1 https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

    * Then, open gvim/vim and run :PluginInstall


Sublime-text-3
--------------
    
     * A list of Sublime-commands is shown at `Sublime Guide <http://sublimeguide.readthedocs.io/en/latest/index.html>`_


    .. code-block:: shell

        sudo add-apt-repository ppa:webupd8team/sublime-text-3 && sudo apt-fast update && sudo apt-fast install sublime-text-installer

    * After installing sublime, press **ctrl+shift+p** and select "**Install Packages Control**"


    * Install packages: **ctrl+shift+p** and select **Package Control: Install Packages**, then **type the package name**
            * print to html, 
            * reStructuredText Improved, 
            * reStructured Text (RST) snippests.
            * Cython syntax support
            * VHDL package for sublime text 
            * SystemVerilog


    * Change tabs-to-spaces : go to "preferences->settings" and replace the existing code with 
     
        .. code-block:: shell
           
           {
            "ignored_packages":
            [
                "RestructuredText",
                "Vintage"
            ],

            "translate_tabs_to_spaces": true,
           }

    * Convert existing 'tabs' to 'spaces : go to "preferences->Key Bindings" and replace the existing code with
    
        .. code-block:: shell
        
            [
                    { "keys": ["ctrl+shift+y"], "command": "expand_tabs", "args" : {"set_translate_tabs" : true} }
            ]

        Now press 'ctrl+shift+y' in the file,  to replace tabs with spaces in the existing files. 

   


Emacs
-----

    .. code-block:: shell

        sudo apt-add-repository ppa:ubuntu-elisp/ppa && sudo apt-fast update && sudo apt-fast install emacs-snapshot 


Unrar
-----

    .. code-block:: shell

        sudo apt-fast install unrar


LibreOffice
------------

    .. code-block:: shell
    
        sudo apt-get install libreoffice


Latex and Texstudio
-------------------
    
    * Latex 
    
        .. code-block:: shell

            sudo apt-fast install texlive-full

    * Texstudio:  
   
        .. code-block:: shell

            sudo apt-fast install texstudio

    * Open Texstudio and 

        * Copy below Makeindex-code and "replace" the existing Makeindex-code from "Option->Configure-TexStudio->Commands->Makeindex". This is required to build the "index" and "list of abbreviations"
        
            .. code-block:: shell

                makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg

        * Download and save :download:`dictionary <download/en_us.oxt>`. Next, add dictionary to latex "Option->Configure-TexStudio->Language Checking"; locate the directory where the dictionary is saved "Spelling Dictionary directories", and selected the dictionary "en_us.oxt".


Zim Desktop
-----------

    * It is required for latex-equation and making notes/checklist etc. 
    
        .. code-block:: shell

            sudo add-apt-repository ppa:jaap.karssenberg/zim && sudo apt-fast update  && sudo apt-fast install zim

    * Then add the equation-plugin. Go to "Edit->preferences->Plugins->select Insert Equation"
    * Then go to "Insert->Equation" to type the equation **(first install latex and then add it)**.
       

GHDL and IVerilog simulator
---------------------------

GHDL and IVerilog simulators can be  used to simulate the VHDL and Verilog codes respectively, 

* GHDL

    .. code-block:: shell
    
        sudo apt-fast install gnat
        git clone --depth 1 https://github.com/ghdl/ghdl
        ./configure --prefix=/usr/local
        sudo make
        sudo make install

    * Execute the code as below, 
    
    .. code-block:: shell
    
        -- syntax check (first design and then testbench)
        ghdl -s <filename.vhd> 

        -- analyse the code (first design and then testbench)
        ghdl -a <filename.vhd>

        -- elaborate the code (testbench)
        ghdl -e <entity-name>

        -- run the code (testbench)
        ghdl -r <entity-name>

        -- run the code and save results in vcd file (testbench)
        ghdl -r <entity-name> --vcd=<filename.vcd>


* IVerilog 
  
    .. code-block:: shell
    
        sudo apt-fast install iverilog

Vivado
------

.. code-block:: shell

    sudo ./xsetup

    (add part to .bashrc)
    export PATH="/opt/Xilinx/Vivado/2017.4/bin:$PATH"

    (if required, change permission for .Xilinx folder)
    sudo chmod -R 777 .Xilinx/

    (run vivado to start)
    vivado



.. _`sec_qmodelsim_ubuntu`: 

Quartus \& Modelsim
-------------------

    .. note:: 

        * These settings are for Quartus 17 (supports only 64 bit OS) and Ubuntu 16.04.4 (64 bit). 
        * See :numref:`sec_qmodelsim_fedora` for installing Quartus 13 (supports both 32 and 64 bit OS) on 32-bit Ubuntu. 

    * Download Quartus 17 (QuartusLite-Versions are free) with Modelsim and devices. 
    * Next, install following libraries on Ubuntu, 
    
    .. warning:: 

        Open "Software updates" and select "main repository" and enable "unstable packages" and "source-code" for this step; as "libc6-dev" etc. may not be available in the other servers' repositories.     
    
    
    .. code-block:: shell
    
        sudo apt-fast install libxft2 libxft2:i386 lib32ncurses5
        sudo apt-fast install libxrender1:i386 libxtst6:i386 libxi6:i386
        sudo apt-fast install libc6-dev 
        ('enable source code repositories' in software sources for below two packages)
        sudo apt-fast build-dep -a i386 libfreetype6
        sudo apt-fast install libc6-dev-i386

        (optional : fonts for modelsim)
        sudo apt-fast install xfonts-75dpi


    * Change the permission for the 'Quatus---.run'  file (i.e. e.g. QuartusLiteSetup-17.0.0.595-linux.run) and then install as shown below, 
      
        .. code-block:: shell

            chmod 777 Quart*
    
    * Install Quartus. Select Free-modelsim option and desired-Devices during installation,   
    
        .. code-block:: shell
        
            ./Quart*

    * Modelsim settings 

        .. code-block:: text
        
            (go to modelsim_ase folder)
            cd 
            

    * Modify 'vco' file i.e. find vco="linux_rh60"  and change it to vco="linux" 
      
        .. code-block:: shell
        
            (go to folder 'modelsim_ase')
            cd intelFPGA_lite/17.0/modelsim_ase/

            (change permission of vco file)
            chmod 777 vco

            (change vco="linux_rh60"  to vco="linux") 
            sudo gedit vco
            
            (go to modelsim_ase/bin folder and run 'vsim')            
            cd bin
            ./vsim

    * Modify '.bashrc' file to run 'modelsim' from any folder, 
    
    .. code-block:: text
    
        cd
        gedit .bashrc

        (add following line at the end. Replace "meher" with correct username)
        export PATH="/home/meher/intelFPGA_lite/17.0/modelsim_ase/bin:$PATH"
	export PATH="/home/meher/intelFPGA_lite/17.0/quartus/bin:$PATH"

	(now open quartus and modelsim using commands 'quartus' and 'vsim' respectively, as shown below)
	$ quartus
	$ vsim

JTAG Settings
^^^^^^^^^^^^^

        * It is required for loading the .sof file on FPGA.
        * Go to rules.d folder and create file with name '51-usbblaster.rules' as below, 

            .. code-block:: shell

                cd /etc/udev/rules.d
                
                (use "xed" in Mint and "gedit" in Ubuntu in below command)

                sudo gedit 51-usbblaster.rules  


        * Next, paste following code to it

            .. code-block:: shell
            
                # Altera USB-Blaster for Quartus FPGA Software
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6001", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6002", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6003", MODE="0666"
                # USB-Blaster II
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6010", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6810", MODE="0666"

        * Connect the FPGA and type command '**lsusb**'; it will show the 'Altera Blaster' as shown below (If not, reboot the computer)
    
            .. code-block:: shell

                lsusb

                (Similar to following result will be displayed, look for the Altera Blaster)

                /etc/udev/rules.d$ lsusb
                Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
                Bus 007 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
                Bus 006 Device 002: ID 03f0:0024 Hewlett-Packard KU-0316 Keyboard
                Bus 006 Device 003: ID 09fb:6001 Altera Blaster


Modelsim-compilation from terminal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        * Create the work directory, 
        
            .. code-block:: shell
            
                vlib work


        * Compile the VHDL or Verilog code using 'vcom' and 'vlog' respectively. 
            
            .. code-block:: shell
            
                vcom <file_name>.vhd
                vlog <file_name>.v

                or 

                vcom *.vhd
                vlog *.v

        * Start the simulator. This will open the Modelsim GUI. 
        
            .. code-block:: shell
            
                vsim <file_name>

        * Simulate the design (but do not run the simulation). 
        * Then follow the steps in the section :ref:`saving_vcd`, to save the results in vcd-file. 


.. _`saving_vcd`:

Saving results in .vcd format using ModelSim
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes it is better to save the simulation results in .vcd format, which can be done using Modelsim as below, 
    
    * Compile and simulate the design (but do not run the simulation)

    * Next create the vcd file at desired location, 
    
        .. code-block:: shell
        
            vcd file /<location for vcd file>/test.vcd e.g. 

            vcd file /home/test.vcd

    * add all waveforms to it, 
      
        .. code-block:: shell
        
            vcd add -r /*

    * Run the simulator using '-all' or desired time, 
    
        .. code-block:: shell
        
            run -all 

            or 

            run 10ms

    * Save the results 
    
        .. code-block:: shell
        
            vcd checkpoint

    * Close modelsim if required, 
    
        .. code-block:: shell
        
            quit -f


.vcd to .wlf conversion
^^^^^^^^^^^^^^^^^^^^^^^

The .vcd files can not be read directly by Modelsim, therefore it is required to convert it into .wlf format, as shown below. Or use :ref:`gtk-wave` to open the .vcd file.  

    * First convert the .vcd format to .wlf format, 
      
        .. code-block:: shell
        
            vcd2wlf  <location of .vcd file>  <location for .wlf file> e.g.

            vcd2wlf  /home/test.vcd  /home/test.wlf

    * Next, open this .wlf file i.e. "Files->Open->select all from drop-down menu->select .wlf file"



Shortcut icons
^^^^^^^^^^^^^^

        * Create three files at desired location with '.desktop' extension and paste the codes as below

        * Quartus.desktop **(use correct quartus location and replace "meher" with correct username)**

            .. code-block:: shell

                [Desktop Entry]
                Type=Application
                Version=0.9.4
                Name=Quartus (Quartus Prime 17.0) Lite Edition
                Comment=Quartus (Quartus Prime 17.0)
                Icon=/home/meher/intelFPGA_lite/17.0/quartus/adm/quartusii.png
                Exec=/home/meher/intelFPGA_lite/17.0/quartus/bin/quartus --64bit
                Terminal=false
                Path=/home/meher/intelFPGA_lite/17.0 
        
            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Quartus.desktop
            

        * Nios.desktop **(use correct nios location and replace <username> with correct username)**

            .. code-block:: shell

                [Desktop Entry]
                Type=Application
                Version=0.9.4
                Name=NiosII
                Comment=NiosII(Quartus Prime 17.0)
                Icon=/home/meher/intelFPGA_lite/17.0/nios2eds/bin/eclipse_nios2/icon.xpm
                Exec=/home/meher/intelFPGA_lite/17.0/nios2eds/bin/eclipse-nios2
                Terminal=false
                Path=/home/meher/intelFPGA_lite/17.0


            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Nios.desktop

        * Modelsim.desktop **(use correct modelsim location and replace <username> with correct username)**
        
            .. code-block:: shell

                [Desktop Entry]
                Type=Application
                Version=0.9.4
                Name=Modelsim
                Comment=ModelSim(Quartus Prime 17.0)
                Icon=/home/meher/intelFPGA_lite/17.0/modelsim_ase/tcl/bitmaps/m.gif
                Exec=/home/meher/intelFPGA_lite/17.0/modelsim_ase/bin/vsim
                Terminal=true
                Path=/home/meher/intelFPGA_lite/17.0

            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Modelsim.desktop



PuTTY (TCP/IP client)
---------------------

    .. code-block:: shell
    
        sudo apt-fast install putty


Anaconda (Python)
-----------------

    * Download correct version of `Anaconda <https://www.anaconda.com/download/#download>`_ i.e. 32 bit or 64 bit with required Python version i.e. 2 or 3. 
        
    * Then run the following commands. And select 'Yes' at the end of installation to set the anaconda as default-python environment.
    
        .. code-block:: shell

            chmod 777 Ana*
        
            ./Ana*

    * Close and reopen the terminal. 
    * Download :download:`requirements.txt <download/requirements.txt>` and run below command to install the additional packages, 

        .. code-block:: shell
        
            (install below for mysqlclient, see MySQL installation as well)
            sudo apt-get install libmysqlclient-dev

            pip install -r requirements.txt

            # if mysqlclient does not connect then use '127.0.0.1' (instead of 'localhost')
            DATABASES = {
                'default': {
                    'ENGINE': 'django.db.backends.mysql',
                    'NAME': 'myproject',
                    'USER': 'meher',
                    'PASSWORD': 'Meher123!',
                    'HOST': '127.0.0.1',
                    'PORT': '',
                }
            }

    * Test 'virtualenv' 
    
        Test the virtualenv. Old version of matplotlib is used, as newer is not installed properly

        .. code-block:: python
        
            virtualenv test -p python3.6

            source test/bin/activate

            pip install matplotlib==1.5.3

    * To unset/set the anaconda as default-python, go to ~/.bashrc file and remove/add following line there, 
      
        .. code-block:: shell
        
            export PATH="<location of anaconda>/bin:$PATH" e.g.
            
            export PATH="/home/<username>/anaconda3/bin:$PATH"



    * Go to <installed-location-anaconda>/bin and rename 'python3' to 'python36' (optional step). Now, we can use following commands to open different python shells, 
    
        .. code-block:: shell
         
            ( open linux-python2 shell)
            $ python2 
            
            ( open anaconda-python3 shell)
            $ python 
            
            ( open linux-python3 shell)
            $ python3
            
        Or we can set any name to start Python by making following changes in .bashrc file, 
        
        .. code-block:: shell
            
            (use "xed" in Mint and "gedit" in Ubuntu in below command)
            gedit .bashrc

            (add following line at the end)

            (open python3 using 'python' command)
            alias python='/usr/bin/python3'

            (open anaconda-python using 'anaconda' command)
            alias anaconda='<installed-location-anaconda>/bin/python'
            

MySql Server
------------

    .. code-block:: shell

        sudo apt-fast install mysql-server

    .. code-block:: shell

        sudo apt-fast install mysql-workbench
 

PostgreSql
----------

.. code-block:: shell

    sudo apt-fast install postgresql postgresql-contrib

    (graphical tool)
    sudo apt-fast install pgadmin3
    
    (commands)
    (login : user 'postgres' is created during installation)
    sudo su - postgres

    (start postgre session)
    psql

    (create database, user and password)
    CREATE DATABASE myproject;
    CREATE USER root WITH PASSWORD 'root';


* pgadmin3 settings 

    .. code-block:: shell

        * in above step we used "CREATE DATABASE myproject;" and "CREATE USER root WITH PASSWORD 'root';"
        * Now open pgadmin3 and fill  following details, 

            - Name : myproject
            - HOST : localhost
            - Port : 5432
            - username : root
            - password : root
            - Group : Servers

        * Above setting will connect to the database. Press 'ctrl-e' to write the queries


C++ and Fortran compiler
------------------------

    .. code-block:: shell

        sudo apt-fast install g++

        (linting tool)
        sudo apt-fast install splint
        

    .. code-block:: shell

        sudo apt-fast install gfortran
 
 
Java
----

    .. code-block:: shell
    
        sudo apt-fast install default-jdk


Qt Designer
-----------

It can be used for designing the 'Desktop user interface'. 

    .. code-block:: shell

        (install qt5, if required)    
        sudo apt-fast install qt5-default

Next, install any one of the two, 

* qt4-designer is a part of IDE which creates only Form. 
* qtcreator is the IDE



    .. code-block:: shell
    
        sudo apt-fast install qt4-designer
        (run below to start qt4-designer)
        designer

        or 

        sudo apt-fast install qtcreator
        (run below to start qtcreator)
        qtcreator


* The '.ui' file can be converted into python as below, 

.. code-block:: shell

    pyuic5 -x input.ui -o output.py


selenium setup
^^^^^^^^^^^^^^

* Go to `Selenium website <https://www.seleniumhq.org/download/>`_ and download "Selenium Standalone Server (jar file)" and "Java \: Selenium Client \& WebDriver Language Bindings (zip file)"
* Extract the files from the zipped folder and copy all the jar-files in one folder (including jar-files in subfolders). 
* Download `Chrome driver <https://sites.google.com/a/chromium.org/chromedriver/downloads>`_ for Selenium. 
* Download `Eclipse IDE for Java EE Developers <https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen3a>`_

Test the setup
^^^^^^^^^^^^^^

* Open Eclipse. Create project 'TestSelenium' and add package 'my_package' to it. Next, add a file 'OpenChrome.java' to it. Copy the below code to it. 
* Provide correct location of 'chromedriver' i.e. replace "/home/meher/Desktop/java_eclipse/driver/chromedriver" with correct path. 

    .. code-block:: java

        package my_package; 
        import org.openqa.selenium.chrome.ChromeDriver;

        public class OpenChrome {
            public static void main (String args []) {
                // location of chrome driver
                System.setProperty("webdriver.chrome.driver", 
                        "/home/meher/Desktop/java_eclipse/driver/chromedriver");
                
                ChromeDriver driver = new ChromeDriver();
                driver.get("https://www.google.com/");
            }
        }
           

* Right click on the 'TestSelenium->Build Path->Configure Build Path->Libraries->Add External Jars'. Add all the jar files and press 'apply and close'.
* Right click on 'OpenChrome.java' and "Run as->Java application". 
* If everything is correct, then Chrome will open. 


Scratch (visual coding)
-----------------------

    .. code-block:: shell
    
        sudo apt-fast install scratch

Simple Screen Recorder
----------------------

    .. code-block:: shell

        sudo add-apt-repository ppa:maarten-baert/simplescreenrecorder && sudo apt-fast update && sudo apt-fast install simplescreenrecorder


Audacious (audio player)
------------------------

  
    .. code-block:: shell

        sudo apt-fast install audacious


VLC (video player and converter)
--------------------------------
  
    .. code-block:: shell

        sudo apt-fast install vlc browser-plugin-vlc

* To convert or clip the videos, 
    *  Go to "Media->Convert/Save" and "Add" the file
    *  To clip the video, 
        *  select "Show more option"
        *  Go to "Edit Options" and type the below code, 
           
        .. code-block:: shell
        
            (modify the start time only)
            :file-caching=300 :start-time=<time in second> e.g.
            :file-caching=300 :start-time=2

            
            (modify both the start time and end time)
            :file-caching=300 :start-time=<time in second> :stop-time<time in second>
            
            (define run-time to run fast)
            :file-caching=300 :start-time=<time in second> :stop-time<time in second> :run-time<time in second>   

        * In the below step, the during conversion the complete time-duration may be displayed, but it will stop after the time which is defined in above step. 

    * Finally, press "Convert/Save" and select the correct "Profile" for conversion. 



KDEnlive video editors
----------------------
 
    *  Use KDEnlive for professional video editing 
    
        .. code-block:: shell

            sudo add-apt-repository ppa:kdenlive/kdenlive-stable && sudo apt-fast update && sudo apt-fast install kdenlive


Furius ISO Mount
----------------
    
    .. code-block:: shell

        sudo apt-fast install furiusisomount


Dictionary
----------
    
    .. code-block:: shell

        sudo apt-fast install artha


Browser
-------
    
    * Chromium: 

        .. code-block:: shell
            
            sudo apt-fast install chromium-browser

    * Google Chrome: download .deb file from website and install
    * Opera: download .deb file from website and install


PDF editors
-----------

    * Okular : it allows highlighting and comments in PDF.

        .. code-block:: shell

            sudo apt-fast install okular

    * PDF-Shuffler (for cropping and PDF-Shuffling): 

        .. code-block:: shell

            sudo apt-fast install pdfshuffler


Indicator-StickyNotes
---------------------

    .. code-block:: shell

        sudo apt-add-repository ppa:umang/indicator-stickynotes && sudo apt-fast update && sudo apt-fast install indicator-stickynotes


.. _`gtk-wave`:

gtk-wave
--------

    * It can be used for viewing VCD-Waveforms i.e. simulation results of FGPA

        .. code-block:: shell

            sudo apt-fast install gtkwave


Typing tutor
------------

    .. code-block:: shell

        sudo apt-fast install klavaro


PCB design
----------

    .. code-block:: shell

        sudo apt-fast install eagle



Bootable USB
------------

    .. code-block:: shell

        (format USB with Fat32, it does not work for bootable-Windows)
        sudo add-apt-repository ppa:gezakovacs/ppa && sudo apt-fast update && sudo apt-fast install unetbootin

        (format USB with NTFS)
        sudo add-apt-repository ppa:nilarimogard/webupd8 && sudo apt-fast update && sudo apt-fast install woeusb

    .. note:: 

            If grub is missing after installing Windows or for some other reasons, then 

            1. Start Ubuntu from 'ubuntu-live-cd' with option 'Try ubuntu'
            
            2. Run below codes, 
            
            .. code-block:: shell
            
                (replace sda1 with correct sda-value i.e. drive where 'Ubuntu' is installed)
                sudo mount /dev/sda1 /mnt   

                (do not write number after 'sda')
                sudo grub-install --root-directory=/mnt/ /dev/sda  

            3. Reboot the system and run system without 'ubuntu-live-cd'
            4. Open terminal and run "sudo update-grub"
            5. Use below to have same time settings in dual boot system
            
            .. code-block:: shell
            
                (to have same time in multiboot system)
                timedatectl set-local-rtc 1

                (use below to remove above settings)
                timedatectl set-local-rtc 0
    


cups-pdf
--------

    * Print to pdf; by default the PDF is saved to location ~/PDF/. This is required to print the pdf through VIM using :hardcopy command
    
        .. code-block:: shell

            sudo apt-fast install cups-pdf

Image editors
-------------

Shutter screenshot program
^^^^^^^^^^^^^^^^^^^^^^^^^^

    * Install Shutter for screenshot and modifying image as paint-brush
  
        .. code-block:: shell

            sudo add-apt-repository ppa:shutter/ppa && sudo apt-fast update && sudo apt-fast install shutter

    * To enable 'edit' option in Shutter, run following command
    
        .. code-block:: shell

            sudo apt-fast install libgoo-canvas-perl

    * The "gnome-web-photo" is used to generate full size image files and thumbnails from HTML files and web pages. To enable screenshots of websites, run following command
      
        .. code-block:: shell
        
            sudo apt-fast install gnome-web-photo

    * To compress the image, press 'ctrl+shift+p', and select 'resize->run'. Enter the width e.g. 500 (width will be filled automatically). Press 'save'. 


Shotwell
^^^^^^^^

Shutter does not provide the rotate-image feature. Shotwell can be used to rotate and crop the image quickly. 

.. code-block:: shell

    sudo add-apt-repository ppa:yg-jensge/shotwell && sudo apt-fast update && sudo apt-fast install shotwell

GIMP image editor
^^^^^^^^^^^^^^^^^

    .. code-block:: shell
    
        sudo add-apt-repository ppa:otto-kesselgulasch/gimp && sudo apt-fast update && sudo apt-fast install gimp


PDF to JPG/PNG conversion
^^^^^^^^^^^^^^^^^^^^^^^^^

Use any combination for conversion as shown below, 

    .. code-block:: shell
    
        convert -density 300 <file_name.pdf> <file_name.jpg>
        convert -density 300 <file_name.pdf> <file_name.png>
        convert -density 300 <file_name.jpg> <file_name.png>
        

.. _`Ultracopier`:

Ultracopier
-----------

    * Incomplete transfer is also displayed on the transferred-location; and it is very hard to find which files are transferred completely. 
    * Ultracopier removes this problem along with various other features i.e. pause/resume copy etc. 
        
        .. code-block:: shell

            sudo apt-fast install ultracopier

    * See `Start-up programs`_ to add the ultracopier and/or other programs in start-up list. 
    * After following the steps in `Start-up programs`_, restart the machine and Ultracopier will appear in the 'task-bar (with floppy icon'.
    * How to use Ultracopier: 
        * Right click on this floppy icon and select "Add copy/moving -> Add copy/transfer/move". 
        * A window will appear, which ask for the directory to be copied, **Close this window to copy the file**. 
        * Now, click on the "More" option and click on the "+" sign and select the "file" option. Next select the file to be copied. 
        * Finally it will ask for the paste location, select the paste-location for the file.  
    * To change the default copy option as 'file (not folder)', right click on 'floppy icon->options->when manaul open->ask source as file'.
    
    
.. _`Start-up programs`:

Start-up programs
-----------------
    
    * Programs can be added to start up in two ways. Here `Ultracopier`_ is added as start-up program,
    
        * Using Terminal : 
            * go to 'autostart' folder (or create it, if not exist) and create a file with desired name (with .desktop extension) as below, 
               
                .. code-block:: shell

                    (try below)            
                    cd ~/.config/autostart/

                    (if folder does not exist, then create one)
                    mkdir -p ~/.config/autostart

                    cd ~/.config/autostart/
                    
                    (use "xed" in Mint and "gedit" in Ubuntu in below command)
                    gedit Ultracopier.desktop


            * And paste the below code to the file. Note that "/user/bin/ultracopier" is the location of the file
        
                .. code-block:: shell
                
                    [Desktop Entry]
                    Type=Application
                    Exec=/usr/bin/ultracopier
                    X-GNOME-Autostart-enabled=true
                    NoDisplay=false
                    Hidden=false
                    Name[en_IN]=Ultracopier
                    Comment[en_IN]=copy manager
                    X-GNOME-Autostart-Delay=0

        * Using "Start-up application"
            * Go to "Menu" and find "Startup Applications"
            * Click on "Add->Custom command" and fill the fields as below,
             
                .. code-block:: shell
                
                    Name          :   Ultracopier
                    Command       :   /user/bin/ultracopier
                    Command       :   copy manager
                    startup delay :   0

            * Restart the machine 


WINE
----

    * It is Windows Program Loader which is used to install .exe file in Linux
    
        .. code-block:: shell

            sudo apt-fast install wine
    
gtkpod
------

    * It can be used to load the songs on IPod. 
    * Open 'gtkpod' and copy the song to IPod and **save all changes**. 
    * If IPod is not detected by 'gtkpod', then restore the IPod using ITune software on Windows. 

    .. code-block:: shell
    
        sudo apt-fast install gtkpod

Firefox Add-ons
---------------

    * `1-Click YouTube Video Downloader <https://addons.mozilla.org/en-US/firefox/addon/1-click-youtube-video-downl/>`_
    * `DownloadThemAll! <https://addons.mozilla.org/en-US/firefox/addon/downthemall/>`_
    * `Firebug <https://addons.mozilla.org/en-US/firefox/addon/firebug/>`_
    * `Firepath <https://addons.mozilla.org/en-US/firefox/addon/firepath/>`_
    * `Selenium IDE <https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/>`_
    * `SQLite Manager <https://addons.mozilla.org/en-US/firefox/addon/sqlite-manager/>`_
    * `Video DownloadHelper <https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/>`_

Thunderbird Add-ons
-------------------

    * Lightning (calendar)


Custom screen resolution
------------------------

Custom screen resolution may be required, when the correct resolution is not available in the list of resolutions. In this section, 1920x1080 resolution is added for the second screen (i.e. monitor connected to the laptop using VGA port), 

1.  First check the available display devices and corresponding screen-resolutions using following command. In the following outputs two displays are shown i.e. 'LVDS1 (i.e. Laptop screen)' and 'VGA1 (i.e. second screen)'. The \* sign shows the currently used resolution. 
  
    .. code-block:: shell
    
        $ xrandr

        Screen 0: minimum 8 x 8, current 2304 x 800, maximum 32767 x 32767
        
        LVDS1 connected primary 1280x800+0+0 (normal left inverted right x axis y axis) 300mm x 190mm
           1280x800      60.01*+
           1024x768      60.00  
           800x600       60.32    56.25  
           640x480       59.94  
           640x400       60.00  

        VGA1 connected 1024x768+1280+0 (normal left inverted right x axis y axis) 0mm x 0mm
           1024x768      60.00* 
           800x600       60.32    56.25  
           848x480       60.00  
           640x480       59.94  

2.  We will add the screen resolution for the VGA1 i.e. the device which is connected to Laptop using VGA port.
3.  For this, run the following command to find the CVT "mode line values" for the given resolution. In the below code, 1920x1080 resolution is used.  

    .. code-block:: shell

        (type the desired resolution below)

        $ cvt 1920 1080

        # 1920x1080 59.96 Hz (CVT 2.07M9) hsync: 67.16 kHz; pclk: 173.00 MHz
        Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync


4. Now add the above 'new mode' as below. 

    .. code-block:: shell
    
        (copy the mode line value from the above step and paste it after '--newmode')

        $ sudo xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync

5. Next, add the new mode to the desired display device, i.e. VGA1 here,
   
    .. code-block:: shell
    
        (replace VGA1 with correct name)

        $ sudo xrandr --addmode VGA1 "1920x1080_60.00"

6. Now go to "System settings->Displays" and select the newly added resolution. 

7. Finally save the settings in the .profile file, so that it will be available on the next start (otherwise we need to perform the above steps on each restart). Paste the commands in steps 4 and 5 (without 'sudo') at the end of the file as shown below, 

    .. code-block:: shell
    
        (use "xed" in Mint and "gedit" in Ubuntu in below command)

        $ gedit ~/.profile

        (and paste the following line at the end of file)

        xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
        xrandr --addmode VGA1 "1920x1080_60.00"


SD card
-------

Sometimes SD/MMC card is not detected. To avoid the problem, 'reinstall udisks2', 

    .. code-block:: shell
    
        sudo apt-fast install --reinstall udisks2

Reduced size PDF
----------------

PDF compression can be done using following commands,

    .. code-block:: shell
    
        (first convert the pdf to ps format)
        pdf2ps file_name.pdf  file_name.ps

        (next convert the ps to pdf again)
        ps2pdf file_name.ps new_file_name.pdf


Change the GRUB in multiboot system
-----------------------------------

New linux installation changes the Boot-menu at the startup (based on newly installed OS). It can be updated using boot-repair e.g. if we want the Boot-menu according to Linux-Mint, then run the below commands in the Linux-Mint and use the default settings, 

.. code-block:: shell

    sudo add-apt-repository ppa:yannubuntu/boot-repair && sudo apt-fast update && sudo apt-fast install boot-repair
    boot-repair


Virtual Box
-----------

    .. code-block:: shell
    
        sudo apt-fast install virtualbox

Calibre
-------

This can be used for reading the PDF, EPUB and mobi etc. formats, 

    .. code-block:: shell

        sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"

NES Game Emulator
-----------------

Emulators for games like :download:`Mario and Tank <download/NES-games.zip>`, 

    .. code-block:: shell

        sudo apt-fast install gfceu
