.. Linux Software documentation master file, created by
   sphinx-quickstart on Mon Oct  2 17:29:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Software installation in Linux
==============================

.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Contents:

   ubuntu
   fedora
   fedora32


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
