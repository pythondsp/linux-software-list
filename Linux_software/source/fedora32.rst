Software installation in Fedora (32 bit) 
========================================


yum-rocket
----------

Install yum-rocket to increase the speed of download-packages using 'yum', 

    * First remove the 'yum-cache' and download the 'yum-rocket'

    .. code-block:: shell

        $ rm -f /var/cache/yum/timedhosts.txt
        $ cd Desktop
        $ git clone --depth 1 https://github.com/ryanuber/yum-rocket

    * Then go to yum-rocket->yum-rocket->rocket.conf and add line 'maxthreads=10' (without quotes)  

    .. code-block:: shell
        :linenos:
        :emphasize-lines: 12

        $ gedit yum-rocket/yum-rocket/rocket.conf

        (add add the line as below)

        # yum-rocket configuration file

        [main]
        enabled=1

        # The maximum number of threads to use for downloading.
        #maxthreads=5
        maxthreads=10

        # Allow YUM to use this many mirror servers for each repository. Setting this
        # option to 1 would effectively match the default YUM functionality.
        #spanmirrors=3

    * Now copy the files from the 'yum-rocket' folder to 'installed-yum' location, 

    .. code-block:: shell

        sudo cp yum-rocket/yum-rocket/rocket.py /usr/lib/yum-plugins
        sudo cp yum-rocket/yum-rocket/rocket.conf /etc/yum/pluginconf.d

    * Next, update the Fedora 

    .. code-block:: shell

        sudo yum update 

Remove username from terminal
-----------------------------

Modify the .bashrc file as below, to remove the username and working directory etc. from the terminal,

    .. code:: shell

        $ cd 
        $ gedit .bashrc

        (add anyone of the following lines at the end)

        # display $ (or # for root)
        export PS1="\$ "
        
        # display user-name $
        export PS1="\u \$ "

        # display user-name@working-dir $
        export PS1="\u@\w \$ "




Nautilus-open-terminal
----------------------

It can be used to open the terminal at desired folder-location. Right click the mouse and select 'open in terminal'. 

    .. code-block:: shell

        sudo yum install nautilus-open-terminal 

Terminator
----------
    
    * Advance terminal that allows split-screen and other features     
     
        .. code-block:: shell

            sudo yum install terminator

Git
---

    .. code-block:: shell

        sudo yum install git


VIM
---

    .. code-block:: shell

        sudo yum install vim gvim

    * Download `.vimrc <https://drive.google.com/open?id=0B9zymgX8PsIXVm1yRjBjYzdlSkE>`_ file from the website and **save to home folder (rename it to `.vimrc' if 'dot' is not added in the name while downloading)**. And comment/uncomment/add plugins as per requirement,
        
    * Install Vundle : This is used for managing plugins
    
        .. code-block:: shell
        
            git clone --depth 1 https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

    * Then, open gvim/vim and run :PluginInstall


Unrar
-----

    .. code-block:: shell

        sudo yum install unar


Latex and Texstudio
-------------------
    
    * Latex 
    
        .. code-block:: shell

            sudo yum install texlive-scheme-full texlive latexmk


    * Texstudio:  
   
        .. code-block:: shell

            sudo yum install texstudio

        .. note:: 

            Use 'sudo' to open the 'texstudio',  
            
            .. code-block:: shell

                $ sudo texstudio    
                
                or 

                $ sudo texstudio <filename.tex>



    * Open Texstudio and 

        * Copy below Makeindex-code and "replace" the existing Makeindex-code from "Option->Configure-TexStudio->Commands->Makeindex". This is required to build the "index" and "list of abbreviations"
        
            .. code-block:: shell

                makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg

        * `Download and save dictionary <https://drive.google.com/file/d/0B9zymgX8PsIXZ1QxN3ROYUZJSWs/view?usp=sharing>`_. Next, add dictionary to latex "Option->Configure-TexStudio->Language Checking"; locate the directory where the dictionary is saved "Spelling Dictionary directories", and selected the dictionary "en_us.oxt".


Zim Desktop
-----------

    * It is required for latex-equation and making notes/checklist etc. 
    
        .. code-block:: shell

            sudo yum install Zim

    * Then add the equation-plugin. Go to "Edit->preferences->Plugins->select Insert Equation"
    * Then go to "Insert->Equation" to type the equation **(first install latex and then add it)**.
  

GHDL and IVerilog simulator
---------------------------

GHDL and IVerilog simulators can be  used to simulate the VHDL and Verilog codes respectively, 

* GHDL

    .. code-block:: shell
    
        sudo apt-fast install ghdl


    * Execute the code as below, 
    
    .. code-block:: shell
    
        -- syntax check (first design and then testbench)
        ghdl -s <filename.vhd> 

        -- analyse the code (first design and then testbench)
        ghdl -a <filename.vhd>

        -- elaborate the code (testbench)
        ghdl -e <entity-name>

        -- run the code (testbench)
        ghdl -r <entity-name>

        -- run the code and save results in vcd file (testbench)
        ghdl -r <entity-name> --vcd=<filename.vcd>

* IVerilog 
  
    .. code-block:: shell
    
        sudo apt-fast install iverilog
     
.. _`sec_qmodelsim_fedora`:

Quartus \& Modelsim
-------------------

    .. note::
    
        * These settings are for Quartus 13 for 32 bit OS.
        * See :numref:`sec_qmodelsim_ubuntu` for installing Quartus 17 on 64-bit OS.


    * Download Quartus 13.0 or 13.1, as these versions contain more device support than higher versions. 
    * Change the permission for the 'Quatus---.run'  file and then install as shown below, 
    
        .. code-block:: shell

            chmod 777 Quart*
            
            ./Quart*

    .. warning:: Do not run by double clicking the '.run' file.

    * Add the quartus in '.bashrc' to run it using 'quartus' command (optional step), 

      .. code-block:: shell

            $ gedit ~/.bashrc 

            (add following line at the end)
            (user correct location for 'quartus') 

            alias quartus="/home/<username>/InstalledApps/altera/13sp1/quartus/bin/quartus"


    .. note:: 

        * Run the quartus, if following error is shown, then install the libpng12.so.0 as below


            .. code-block:: shell
                
                (Quartus: error while loading shared libraries: libpng12.so.0: cannot open shared object file: No such file or directory)

                sudo yum install libpng12.so.0 


JTAG Settings
^^^^^^^^^^^^^

        * It is required for loading the .sof file on FPGA.
        * Go to rules.d folder and create file with name '51-usbblaster.rules' as below, 

            .. code-block:: shell

                cd /etc/udev/rules.d
                
                sudo gedit 51-usbblaster.rules  


        * Next, paste following code to it

            .. code-block:: shell
            
                # Altera USB-Blaster for Quartus FPGA Software
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6001", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6002", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6003", MODE="0666"
                # USB-Blaster II
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6010", MODE="0666"
                SUBSYSTEMS=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6810", MODE="0666"

        * Connect the FPGA and type command '**lsusb**'; it will show the 'Altera Blaster' as shown below (If not, reboot the computer)
    
            .. code-block:: shell

                lsusb

                (Similar to following result will be displayed, look for the Altera Blaster)

                /etc/udev/rules.d$ lsusb
                Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
                Bus 007 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
                Bus 006 Device 002: ID 03f0:0024 Hewlett-Packard KU-0316 Keyboard
                Bus 006 Device 003: ID 09fb:6001 Altera Blaster

Modelsim setting
^^^^^^^^^^^^^^^^

        * **Do not forget to enter the correct path in below instructions e.g. replace ~/InstalledApps/altera/13sp1/ with correct installed location**

        * `Download and unzip the freetype-2.4.12.tar.bz2 <https://drive.google.com/file/d/0B9zymgX8PsIXM1NlcEtpdU9icDA/view?usp=sharing>`_ file. 
        * Then go to unzip folder and run following commands. 

            .. code-block:: shell

                ./configure --build=i686-pc-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32"
       
                make -j8

        * Now go to Altera modelsim folder **(go to correct installed location)**
          
            .. code-block:: shell
            
                cd ~/InstalledApps/altera/13sp1/modelsim_ase/


        * And run following commands (note that unzip folder is on the Destkop) **(use correct unzip location)**

            .. code-block:: shell

                mkdir lib32

                cp ~/Desktop/freetype-2.4.12/objs/.libs/libfreetype.so* ./lib32

                chmod u+w vco

        * Next modify the vco file, open it with gedit/sublime-text **(use correct vco location)**
          
            .. code-block:: shell
            
                gedit ~/InstalledApps/altera/13sp1/modelsim_ase/vco
    
        * Then find the line "dir=`dirname $arg0`" and add following line below it, **(use correct lib32 location)**
        
            .. code-block:: shell

                (replace <username> with correct name)

                export LD_LIBRARY_PATH=/home/<username>/InstalledApps/altera/13sp1/modelsim_ase/lib32


        * Next edit the vsim file, 
        
            * Open vsim
            
                .. code-block:: shell
                
                    gedit ~/InstalledApps/altera/13sp1/modelsim_ase/bin/vsim


            * change **vco="linux_rh60" to vco="linux"**    

        * Then go to "modelsim_ase/bin/" and run modelsim
    
            .. code-block:: shell

                cd ~/InstalledApps/altera/13sp1/modelsim_ase/bin/

                ./vsim
                
        * If following error is displayed then install "libncurses.so.5"

            .. code-block:: shell

                error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory

            .. code-block:: shell

                sudo yum install libncurses.so.5


    * To run the modelsim from the terminal, run following commands
        
        .. code-block:: shell
            
            cd 
            gedit .bashrc

            # and paste the below line at the end with correct location and username

            export PATH="/home/<username>/InstalledApps/altera/13sp1/modelsim_ase/bin:$PATH"

        Now run the modelsim using 'vsim' (not ./vsim) as below, 

            .. code-block:: shell
            
                vsim


Modelsim-compilation from terminal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        * Create the work directory, 
        
            .. code-block:: shell
            
                vlib work


        * Compile the VHDL or Verilog code using 'vcom' and 'vlog' respectively. 
            
            .. code-block:: shell
            
                vcom <file_name>.vhd
                vlog <file_name>.v

                or 

                vcom *.vhd
                vlog *.v

        * Start the simulator. This will open the Modelsim GUI. 
        
            .. code-block:: shell
            
                vsim <file_name>

        * Simulate the design (but do not run the simulation). 
        * Then follow the steps in the section :ref:`saving_vcd_fedora`, to save the results in vcd-file. 


.. _`saving_vcd_fedora`:

Saving results in .vcd format using ModelSim
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes it is better to save the simulation results in .vcd format, which can be done using Modelsim as below, 
    
    * Compile and simulate the design (but do not run the simulation)

    * Next create the vcd file at desired location, 
    
        .. code-block:: shell
        
            vcd file /<location for vcd file>/test.vcd e.g. 

            vcd file /home/test.vcd

    * add all waveforms to it, 
      
        .. code-block:: shell
        
            vcd add -r /*

    * Run the simulator using '-all' or desired time, 
    
        .. code-block:: shell
        
            run -all 

            or 

            run 10ms

    * Save the results 
    
        .. code-block:: shell
        
            vcd checkpoint

    * Close modelsim if required, 
    
        .. code-block:: shell
        
            quit -f


.vcd to .wlf conversion
^^^^^^^^^^^^^^^^^^^^^^^

The .vcd files can not be read directly by Modelsim, therefore it is required to convert it into .wlf format, as shown below. Or use :ref:`gtk-wave` to open the .vcd file.  

    * First convert the .vcd format to .wlf format, 
      
        .. code-block:: shell
        
            vcd2wlf  <location of .vcd file>  <location for .wlf file> e.g.

            vcd2wlf  /home/test.vcd  /home/test.wlf

    * Next, open this .wlf file i.e. "Files->Open->select all from drop-down menu->select .wlf file"



Shortcut icons
^^^^^^^^^^^^^^

        * Create three files at desired location with '.desktop' extension and paste the codes as below

        * Quartus.desktop **(use correct quartus location and replace <username> with correct username)**

            .. code-block:: shell

                # Quartus   
                [Desktop Entry]   
                Type=Application   
                Version=0.9.4   
                Name=Quartus II 13.0sp1 (32-bit) Web Edition   
                Comment=Quartus II 13.0sp1 (32-bit)   
                Icon=/home/<username>/InstalledApps/altera/13sp1/quartus/adm/quartusii.png   
                Exec=/home/<username>/InstalledApps/altera/13sp1/quartus/bin/quartus   
                Terminal=false   
                Path=/home/<username>/InstalledApps/altera/13sp1  
        
            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Quartus.desktop
            

        * Nios.desktop **(use correct nios location and replace <username> with correct username)**

            .. code-block:: shell

                # NiosII
                [Desktop Entry]
                Type=Application
                Version=0.9.4
                Name=NiosII
                Comment=NiosII(32-bit)
                Icon=/home/<username>/InstalledApps/altera/13sp1/nios2eds/bin/eclipse_nios2/icon.xpm
                Exec=/home/<username>/InstalledApps/altera/13sp1/nios2eds/bin/eclipse-nios2
                Terminal=false
                Path=/home/<username>/InstalledApps/altera/13sp1


            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Nios.desktop

        * Modelsim.desktop **(use correct modelsim location and replace <username> with correct username)**
        
            .. code-block:: shell

                # Modelsim
                [Desktop Entry]
                Type=Application
                Version=0.9.4
                Name=ModelSim
                Comment=ModelSim(32-bit)
                Icon=/home/<username>/InstalledApps/altera/13sp1/modelsim_ase/tcl/bitmaps/m.gif
                Exec=/home/<username>/InstalledApps/altera/13sp1/modelsim_ase/bin/vsim
                Terminal=true
                Path=/home/<username>/InstalledApps/altera/13sp1

            * change the permission
            
                .. code-block:: shell
                
                    chmod 777 Modelsim.desktop


Add application to favorites
----------------------------

Copy the 'shortcut icon (i.e. .desktop file with execute permission)' inside the /usr/share/applications/ folder. In this way, the application will be added to "Application bar" from where we can drag it to 'Favorites bar'. 


PuTTY (TCP/IP client)
---------------------

    .. code-block:: shell
    
        sudo yum install putty


Anaconda (Python)
-----------------

    * Download correct version of `Anaconda <https://www.anaconda.com/download/#download>`_ i.e. 32 bit or 64 bit with required Python version i.e. 2 or 3. 
        
    * Then run the following commands. And select 'Yes' at the end of installation to set the anaconda as default-python environment.
    
        .. code-block:: shell

            chmod 777 Ana*
        
            ./Ana*

    * Close and reopen the terminal. 
    * To unset/set the anaconda as default-python, go to ~/.bashrc file and remove/add following line there, 
      
        .. code-block:: shell
        
            export PATH="<location of anaconda>/bin:$PATH" e.g.
            
            export PATH="/home/<username>/anaconda3/bin:$PATH"


    * Install mysql-connector to connect with MySql using Python
    
        .. code-block:: shell
        
            conda install mysql-connector-python

    * Install ReadTheDoc theme for Sphinx
    
        .. code-block:: shell
        
            conda install sphinx_rtd_theme
    
    * Install 'MyHDHL' using 'pip' (not conda)   

        .. code-block:: shell
        
            pip install myhdl

    * Go to <installed-location-anaconda>/bin and rename 'python3' to 'python36' (optional step). Now, we can use following commands to open different python shells, 
    
        .. code-block:: shell
         
            ( open linux-python2 shell)
            $ python2 
            
            ( open anaconda-python3 shell)
            $ python 
            
            ( open linux-python3 shell)
            $ python3
            
        Or we can set any name to start Python by making following changes in .bashrc file, 
        
        .. code-block:: shell
            
            gedit .bashrc

            (add following line at the end)

            (open python3 using 'python' command)
            alias python='/usr/bin/python3'

            (open anaconda-python using 'anaconda' command)
            alias anaconda='<installed-location-anaconda>/bin/python'
            
MySql Server
------------

    .. code-block:: shell

        sudo yum install mysql-server

    .. code-block:: shell

        sudo yum install mysql-workbench
 

C++ and Fortran compiler
------------------------

    .. code-block:: shell

        sudo yum install gcc-c++

    .. code-block:: shell

        sudo yum install gcc-gfortran
 

Scratch (visual coding)
-----------------------

    .. code-block:: shell
    
        sudo yum install scratch

Simple Screen Recorder
----------------------

    .. code-block:: shell

        sudo yum install simplescreenrecorder


Audacious (audio player)
------------------------

  
    .. code-block:: shell

        sudo yum install audacious


ISO Mount
---------
    
    .. code-block:: shell

        (create the location for mount e..g 'disk')
        sudo mkdir -p /mnt/disk

        (mount the image)
        sudo mount -o loop /<location.iso> /mnt/disk

        (unmount the image)
        sudo umount /mnt/disk


Dictionary
----------
    
    .. code-block:: shell

        sudo yum install artha


PDF editors
-----------

    * Okular : it allows highlighting and comments in PDF.

        .. code-block:: shell

            sudo yum install okular

    * PDF-Shuffler (for cropping and PDF-Shuffling): 

        .. code-block:: shell

            sudo yum install pdfshuffler


.. _`gtk-wave_fedora32`:

gtk-wave
--------

    * It can be used for viewing VCD-Waveforms i.e. simulation results of FGPA

        .. code-block:: shell

            sudo yum install gtkwave


Typing tutor
------------

    .. code-block:: shell

        sudo yum install klavaro


UNetbootin (Bootable USB)
-------------------------

    .. code-block:: shell

       sudo yum install unetbootin 

cups-pdf
--------

    * Print to pdf; by default the PDF is saved to location ~/PDF/. This is required to print the pdf through VIM using :hardcopy command
    
        .. code-block:: shell

            sudo yum install cups-pdf

Image editors
-------------

Shutter screenshot program
^^^^^^^^^^^^^^^^^^^^^^^^^^

    * Install Shutter for screenshot and modifying image as paint-brush
  
        .. code-block:: shell

            sudo yum install shutter

    * To compress the image, press 'ctrl+shift+p', and select 'resize->run'. Enter the width e.g. 500 (width will be filled automatically). Press 'save'. 


Shotwell
^^^^^^^^

Shutter does not provide the rotate-image feature. Shotwell can be used to rotate and crop the image quickly. 

.. code-block:: shell

    sudo yum install shotwell

GIMP image editor
^^^^^^^^^^^^^^^^^

    .. code-block:: shell
    
        sudo yum install gimp


PDF to JPG/PNG conversion
^^^^^^^^^^^^^^^^^^^^^^^^^

Use any combination for conversion as shown below, 

    .. code-block:: shell
    
        convert -density 300 <file_name.pdf> <file_name.jpg>
        convert -density 300 <file_name.pdf> <file_name.png>
        convert -density 300 <file_name.jpg> <file_name.png>
        

gtkpod
------

    * It can be used to load the songs on IPod. 
    * Open 'gtkpod' and copy the song to IPod and **save all changes**. 
    * If IPod is not detected by 'gtkpod', then restore the IPod using ITune software on Windows. 

    .. code-block:: shell
    
        sudo yum install gtkpod

Firefox Add-ons
---------------

    * Video DownloadHelper
    * DownloadThemAll
    * Firebug
    * Firepath
    * SQLite Manager
    * Selenium IDE

Thunderbird Add-ons
-------------------

    * MinimizeToTray revived (install and change preferences)
    * Lightning (calendar)
    * Lightning month tab (to display more than one month)
    * Birthday reminder


Custom screen resolution
------------------------

.. note:: 

    * Login to Fedora with 'Gnome on Xorg' option, otherwise shutter will not work for capturing image. 
    * Or, disable WAYLAND0 as shown in hte below process.

Custom screen resolution may be required, when the correct resolution is not available in the list of resolutions. In this section, 1920x1080 resolution is added for the second screen (i.e. monitor connected to the laptop using VGA port), 


1.  First check the available display devices and corresponding screen-resolutions using following command. In the following outputs two displays are shown i.e. 'LVDS1 (i.e. Laptop screen)' and 'VGA-1 (i.e. second screen)'. The \* sign shows the currently used resolution. 
  
    .. code-block:: shell
    
        $ xrandr

        Screen 0: minimum 8 x 8, current 2304 x 800, maximum 32767 x 32767
        
        LVDS1 connected primary 1280x800+0+0 (normal left inverted right x axis y axis) 300mm x 190mm
           1280x800      60.01*+
           1024x768      60.00  
           800x600       60.32    56.25  
           640x480       59.94  
           640x400       60.00  

        VGA-1 connected 1024x768+1280+0 (normal left inverted right x axis y axis) 0mm x 0mm
           1024x768      60.00* 
           800x600       60.32    56.25  
           848x480       60.00  
           640x480       59.94  


    .. note:: 

        If WAYLAND0 is shown at the output of the 'xrandr' command (instead of LVDS or VGA-1 etc.), then disable 'WAYLAND' as below,  

        .. code-block:: shell

            $ gedit /etc/gdm/custom.conf

            (uncomment the below line i.e. remove '#')

            WaylandEnable=false


2.  We will add the screen resolution for the VGA-1 i.e. the device which is connected to Laptop using VGA port.
3.  For this, run the following command to find the CVT "mode line values" for the given resolution. In the below code, 1920x1080 resolution is used.  

    .. code-block:: shell

        (type the desired resolution below)

        $ cvt 1920 1080

        # 1920x1080 59.96 Hz (CVT 2.07M9) hsync: 67.16 kHz; pclk: 173.00 MHz
        Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync


4. Now add the above 'new mode' as below. 

    .. code-block:: shell
    
        (copy the mode line value from the above step and paste it after '--newmode')

        $ sudo xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync

5. Next, add the new mode to the desired display device, i.e. VGA-1 here,
   
    .. code-block:: shell
    
        (replace VGA-1 with correct name)

        $ sudo xrandr --addmode VGA-1 "1920x1080_60.00"

6. Now go to "System settings->Displays" and select the newly added resolution. 

7. Finally save the settings in the .profile file, so that it will be available on the next start (otherwise we need to perform the above steps on each restart). Paste the commands in steps 4 and 5 (without 'sudo') at the end of the file as shown below, 

    .. code-block:: shell
    
        $ gedit ~/.profile

        (and paste the following line at the end of file)

        xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
        xrandr --addmode VGA-1 "1920x1080_60.00"


Reduced size PDF
----------------

PDF compression can be done using following commands,

    .. code-block:: shell
    
        (first convert the pdf to ps format)
        pdf2ps file_name.pdf  file_name.ps

        (next convert the ps to pdf again)
        ps2pdf file_name.ps new_file_name.pdf
