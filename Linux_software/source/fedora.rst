Software installation in Fedora
===============================


Update packages
---------------

Use 'dnf' or 'yum' to install the packages. 

    .. code-block:: shell
	
    	sudo dnf update && sudo dnf upgrade
    	or
        sudo yum update && sudo dnf upgrade

Avoid updating packages

    .. code-block:: shell
    
        sudo gvim /etc/dnf/dnf.conf

        (add below line)
        exclude=mysql* otherPackages

    
Google chrome
-------------

Go to Activities->Software and activate the third-party packages. Search Google-chrome and install. 


Update grub
-----------

Update grub if required

    .. code-block:: shell
	
	sudo grub2-mkconfig -o /boot/grub2/grub.cfg

Show the desktop
----------------

To enable it, go to Activities->Keyboard->Hide all normal widows->set keys 'super+shift+h'. 


Remove username from terminal
-----------------------------

Modify the .bashrc file as below, to remove the username and working directory etc. from the terminal,

    .. code:: shell

        $ cd 
        $ gedit .bashrc

        (add anyone of the following lines at the end)

        # display $ (or # for root)
        export PS1="\$ "
        
        # display user-name $
        export PS1="\u \$ "

        # display user-name@working-dir $
        export PS1="\u@\w \$ "


Anaconda (Python)
-----------------

    * Download correct version of `Anaconda <https://www.anaconda.com/download/#download>`_ i.e. 32 bit or 64 bit with required Python version i.e. 2 or 3. 
        
    * Then run the following commands. And select 'Yes' at the end of installation to set the anaconda as default-python environment.
    
        .. code-block:: shell

            chmod 777 Ana*
        
            ./Ana*

    * Close and reopen the terminal. 
    * Download :download:`requirements.txt <download/requirements.txt>` and run below command to install the additional packages, 

        .. code-block:: shell
        
            (install below for mysqlclient, see MySQL installation as well)
            sudo dnf install mariadb-devel
            sudo dnf install python python-devel mysql-devel redhat-rpm-config gcc


            pip install -r requirements.txt

            # if mysqlclient does not connect then use '127.0.0.1' (instead of 'localhost')
            DATABASES = {
                'default': {
                    'ENGINE': 'django.db.backends.mysql',
                    'NAME': 'myproject',
                    'USER': 'meher',
                    'PASSWORD': 'Meher123!',
                    'HOST': '127.0.0.1',
                    'PORT': '',
                }
            }
	
    * To unset/set the anaconda as default-python, go to ~/.bashrc file and remove/add following line there, 
      
        .. code-block:: shell
        
            export PATH="<location of anaconda>/bin:$PATH" e.g.
            
            export PATH="/home/<username>/anaconda3/bin:$PATH"


    * Go to <installed-location-anaconda>/bin and rename 'python3' to 'python36' (optional step). Now, we can use following commands to open different python shells, 
    
        .. code-block:: shell
         
            ( open linux-python2 shell)
            $ python2 
            
            ( open anaconda-python3 shell)
            $ python 
            
            ( open linux-python3 shell)
            $ python3
            
        Or we can set any name to start Python by making following changes in .bashrc file, 
        
        .. code-block:: shell
            
            gedit .bashrc

            (add following line at the end)

            (open python3 using 'python' command)
            alias python='/usr/bin/python3'

            (open anaconda-python using 'anaconda' command)
            alias anaconda='<installed-location-anaconda>/bin/python'
  


Terminator
----------
    
    * Advance terminal that allows split-screen and other features     
     
        .. code-block:: shell

            sudo yum install terminator

Git
---

    Git is preinstalled in Fedora. 

    .. code-block:: shell

        sudo yum install git


VIM
---

    .. code-block:: shell

        sudo yum install vim gvim

    *  Download :download:`.vimrc <download/vimrc.txt>` file and **save to home folder (rename it to `.vimrc')**. And comment/uncomment/add plugins as per requirement,
        
    * Install Vundle : This is used for managing plugins
    
        .. code-block:: shell
        
            git clone --depth 1 https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

    * Then, open gvim/vim and run :PluginInstall


Unrar
-----

    .. code-block:: shell

        sudo yum install unar


Latex and Texstudio
-------------------
    
    * Latex 
    
        .. code-block:: shell

            sudo yum install texlive-scheme-full texlive latexmk


    * Texstudio:  
   
        .. code-block:: shell

            sudo yum install texstudio

        .. note:: 

            Use 'sudo' to open the 'texstudio',  
            
            .. code-block:: shell

                $ texstudio    
                
                or 

                $ texstudio <filename.tex>



    * Open Texstudio and 

        * Copy below Makeindex-code and "replace" the existing Makeindex-code from "Option->Configure-TexStudio->Commands->Makeindex". This is required to build the "index" and "list of abbreviations"
        
            .. code-block:: shell

                makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg


Sublime text
------------

    .. code-block:: shell
        
        sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg && sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo && sudo dnf install sublime-text
	
    
    * After installing sublime, press **ctrl+shift+p** and select "**Install Packages Control**"


    * Install packages: **ctrl+shift+p** and select **Package Control: Install Packages**, then **type the package name**
            * print to html, 
            * reStructuredText Improved, 
            * reStructured Text (RST) snippests.
            * Cython syntax support
            * VHDL package for sublime text 
            * SystemVerilog


    * Change tabs-to-spaces : go to "preferences->settings" and replace the existing code with 
     
        .. code-block:: shell
           
           {
            "ignored_packages":
            [
                "RestructuredText",
                "Vintage"
            ],

            "translate_tabs_to_spaces": true,
           }

    * Convert existing 'tabs' to 'spaces : go to "preferences->Key Bindings" and replace the existing code with
    
        .. code-block:: shell
        
            [
                    { "keys": ["ctrl+shift+y"], "command": "expand_tabs", "args" : {"set_translate_tabs" : true} }
            ]

        Now press 'ctrl+shift+y' in the file,  to replace tabs with spaces in the existing files. 

	
Zim Desktop
-----------

    * It is required for latex-equation and making notes/checklist etc. 
    
        .. code-block:: shell

            sudo yum install Zim

    * Then add the equation-plugin. Go to "Edit->preferences->Plugins->select Insert Equation"
    * Then go to "Insert->Equation" to type the equation **(first install latex and then add it)**.
  


Add application to favorites
----------------------------

Copy the 'shortcut icon (i.e. .desktop file with execute permission)' inside the /usr/share/applications/ folder. In this way, the application will be added to "Application bar" from where we can drag it to 'Favorites bar'. 


PuTTY (TCP/IP client)
---------------------

    .. code-block:: shell
    
        sudo yum install putty

Postgre SQL
-----------

* Installation 

    .. code-block:: shell

        sudo yum install postgresql-server postgresql-contrib

        (graphical tool)
        sudo yum install pgadmin3

        (commands)
        sudo su - postgres  
        psql
        CREATE DATABASE myproject;
        CREATE USER root WITH PASSWORD 'root'



* Settings for Django

    .. code-block:: shell

        (initialize server)
        sudo postgresql-setup initdb

        (start service)
        sudo systemctl start postgresql

        (make server available for users other than system-users e.g. Django-applications)
        (change 'indent' to 'md5' as shown below)
        sudo vim /var/lib/pgsql/data/pg_hba.conf

        # TYPE  DATABASE        USER            ADDRESS                 METHOD

        # "local" is for Unix domain socket connections only
        local   all             all                                     peer
        # IPv4 local connections:
        #host    all             all             127.0.0.1/32            ident
        host    all             all             127.0.0.1/32            md5
        # IPv6 local connections:
        #host    all             all             ::1/128                 ident
        host    all             all             ::1/128                 md5
        # Allow replication connections from localhost, by a user with the
        # replication privilege.
        #local   replication     postgres                                peer
        #host    replication     postgres        127.0.0.1/32            ident
        #host    replication     postgres        ::1/128                 ident


        (restart and enable server)
        sudo systemctl restart postgresql
        sudo systemctl enable postgresql

        (login : user 'postgres' is created during installation)
        sudo su - postgres

        (start postgre session)
        psql

        (create database, user and password)
        CREATE DATABASE myproject;
        CREATE USER root WITH PASSWORD 'root';
 

* pgadmin3 settings 

    .. code-block:: shell

        * in above step we used "CREATE DATABASE myproject;" and "CREATE USER root WITH PASSWORD 'root';"
        * Now open pgadmin3 and fill  following details, 

            - Name : myproject
            - HOST : localhost
            - Port : 5432
            - username : root
            - password : root
            - Group : Servers

        * Above setting will connect to the database. Press 'ctrl-e' to write the queries


MySQL (version 80, Fedora 27)
-----------------------------

.. code-block:: shell

    sudo dnf install https://dev.mysql.com/get/mysql57-community-release-fc27-10.noarch.rpm

    (install mysql server and workbench)
    sudo dnf install mysql-community-server mysql-workbench-community
    
    (start and enable service)
    systemctl start mysqld.service 
    systemctl restart mysqld.service
    systemctl enable mysqld.service

    (see the temporary password generated during installation)
    grep 'A temporary password is generated for root@localhost' /var/log/mysqld.log |tail -1

    (run below command and enter temporary password. After that it will ask for new password)
    (new password should be atleast 8 characters with Capital letter, number and special character)
    (e.g. Meher123!)
    /usr/bin/mysql_secure_installation

    (start mysql)
    mysql -h localhost -u root -p


    (install below for mysqlclient, see MySQL installation as well)
    sudo dnf install mariadb-devel
    sudo dnf install python python-devel mysql-devel redhat-rpm-config gcc

    pip install mysqlclient


* Create database, user and give permission to users to access the database, 

.. code-block:: shell

    $ mysql -u root -p

    CREATE DATABASE myproject;
    CREATE USER 'meher' IDENTIFIED BY 'Password123!';
    GRANT ALL ON myproject.* TO 'meher';
    FLUSH PRIVILEGES;

    # if mysqlclient does not connect then use '127.0.0.1' (instead of 'localhost')
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'myproject',
            'USER': 'meher',
            'PASSWORD': 'Password123!',
            'HOST': '127.0.0.1',
            'PORT': '',
        }
    }


C++ and Fortran compiler
------------------------

    .. code-block:: shell

        sudo yum install gcc-c++

    .. code-block:: shell

        sudo yum install gcc-gfortran
 

VLC
---

.. code-block:: shell

    sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm && sudo dnf install vlc


Audacious (audio player)
------------------------

  
    .. code-block:: shell

        sudo yum install audacious


ISO Mount
---------
    
    .. code-block:: shell

        (create the location for mount e..g 'disk')
        sudo mkdir -p /mnt/disk

        (mount the image)
        sudo mount -o loop /<location.iso> /mnt/disk

        (unmount the image)
        sudo umount /mnt/disk


Dictionary
----------
    
    .. code-block:: shell

        sudo yum install artha


PDF editors
-----------

    * Okular : it allows highlighting and comments in PDF.

        .. code-block:: shell

            sudo yum install okular

    * PDF-Shuffler (for cropping and PDF-Shuffling): 

        .. code-block:: shell

            sudo yum install pdfshuffler


.. _`gtk-wave_fedora`:

gtk-wave
--------

    * It can be used for viewing VCD-Waveforms i.e. simulation results of FGPA

        .. code-block:: shell

            sudo yum install gtkwave


UNetbootin (Bootable USB)
-------------------------

    .. code-block:: shell

       sudo yum install unetbootin 

cups-pdf
--------

    * Print to pdf; by default the PDF is saved to location ~/PDF/. This is required to print the pdf through VIM using :hardcopy command
    
        .. code-block:: shell

            sudo yum install cups-pdf

Image editors
-------------

Shutter screenshot program
^^^^^^^^^^^^^^^^^^^^^^^^^^

    * Install Shutter for screenshot and modifying image as paint-brush

    .. note:: 

        * Login to Fedora with 'Gnome on Xorg' option, otherwise shutter will not work for capturing image. 
        * Otherwise use inbuilt 'Screenshot' tool for capturing images and then use 'shutter' to modify the image. Using 'shift+print-screen' to select the area for screenshot. Image will be saved in /pictures/ folder

  
        .. code-block:: shell

            sudo yum install shutter

    * To compress the image, press 'ctrl+shift+p', and select 'resize->run'. Enter the width e.g. 500 (width will be filled automatically). Press 'save'. 


Shotwell
^^^^^^^^

Shutter does not provide the rotate-image feature. Shotwell can be used to rotate and crop the image quickly. 

.. code-block:: shell

    sudo yum install shotwell

GIMP image editor
^^^^^^^^^^^^^^^^^

    .. code-block:: shell
    
        sudo yum install gimp


PDF to JPG/PNG conversion
^^^^^^^^^^^^^^^^^^^^^^^^^

Use any combination for conversion as shown below, 

    .. code-block:: shell
    
        convert -density 300 <file_name.pdf> <file_name.jpg>
        convert -density 300 <file_name.pdf> <file_name.png>
        convert -density 300 <file_name.jpg> <file_name.png>
        

gtkpod
------

    * It can be used to load the songs on IPod. 
    * Open 'gtkpod' and copy the song to IPod and **save all changes**. 
    * If IPod is not detected by 'gtkpod', then restore the IPod using ITune software on Windows. 

    .. code-block:: shell
    
        sudo yum install gtkpod

Firefox Add-ons
---------------

    * Video DownloadHelper
    * DownloadThemAll
    * Firebug
    * Firepath
    * SQLite Manager
    * Selenium IDE

Thunderbird Add-ons
-------------------

    * MinimizeToTray revived (install and change preferences)
    * Lightning (calendar)
    * Lightning month tab (to display more than one month)
    * Birthday reminder


Custom screen resolution
------------------------

.. note:: 

    * Login to Fedora with 'Gnome on Xorg' option, otherwise shutter will not work for capturing image. 
    * Or, disable WAYLAND0 as shown in hte below process.

Custom screen resolution may be required, when the correct resolution is not available in the list of resolutions. In this section, 1920x1080 resolution is added for the second screen (i.e. monitor connected to the laptop using VGA port), 


1.  First check the available display devices and corresponding screen-resolutions using following command. In the following outputs two displays are shown i.e. 'LVDS1 (i.e. Laptop screen)' and 'VGA-1 (i.e. second screen)'. The \* sign shows the currently used resolution. 
  
    .. code-block:: shell
    
        $ xrandr

        Screen 0: minimum 8 x 8, current 2304 x 800, maximum 32767 x 32767
        
        LVDS1 connected primary 1280x800+0+0 (normal left inverted right x axis y axis) 300mm x 190mm
           1280x800      60.01*+
           1024x768      60.00  
           800x600       60.32    56.25  
           640x480       59.94  
           640x400       60.00  

        VGA-1 connected 1024x768+1280+0 (normal left inverted right x axis y axis) 0mm x 0mm
           1024x768      60.00* 
           800x600       60.32    56.25  
           848x480       60.00  
           640x480       59.94  


    .. note:: 

        If WAYLAND0 is shown at the output of the 'xrandr' command (instead of LVDS or VGA-1 etc.), then disable 'WAYLAND' as below,  

        .. code-block:: shell

            $ gedit /etc/gdm/custom.conf

            (uncomment the below line i.e. remove '#')

            WaylandEnable=false


2.  We will add the screen resolution for the VGA-1 i.e. the device which is connected to Laptop using VGA port.
3.  For this, run the following command to find the CVT "mode line values" for the given resolution. In the below code, 1920x1080 resolution is used.  

    .. code-block:: shell

        (type the desired resolution below)

        $ cvt 1920 1080

        # 1920x1080 59.96 Hz (CVT 2.07M9) hsync: 67.16 kHz; pclk: 173.00 MHz
        Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync


4. Now add the above 'new mode' as below. 

    .. code-block:: shell
    
        (copy the mode line value from the above step and paste it after '--newmode')

        $ sudo xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync

5. Next, add the new mode to the desired display device, i.e. VGA-1 here,
   
    .. code-block:: shell
    
        (replace VGA-1 with correct name)

        $ sudo xrandr --addmode VGA-1 "1920x1080_60.00"

6. Now go to "System settings->Displays" and select the newly added resolution. 

7. Finally save the settings in the .profile file, so that it will be available on the next start (otherwise we need to perform the above steps on each restart). Paste the commands in steps 4 and 5 (without 'sudo') at the end of the file as shown below, 

    .. code-block:: shell
    
        $ gedit ~/.profile

        (and paste the following line at the end of file)

        xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
        xrandr --addmode VGA-1 "1920x1080_60.00"


Reduced size PDF
----------------

PDF compression can be done using following commands,

    .. code-block:: shell
    
        (first convert the pdf to ps format)
        pdf2ps file_name.pdf  file_name.ps

        (next convert the ps to pdf again)
        ps2pdf file_name.ps new_file_name.pdf


Partition manager
-----------------

    .. code-block:: shell
    
        sudo yum install gparted

Tree
----

    * 'tree' command can be useful see the directory structure, 
    
        .. code-block:: shell
        
            sudo yum install tree

Scratch (visual coding)
-----------------------

    .. code-block:: shell
    
        sudo yum install scratch


Simple Screen Recorder
----------------------

    .. code-block:: shell

        sudo rpm -Uvh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm
        sudo dnf install simplescreenrecorder

Calibre
-------

This can be used for reading the PDF, EPUB and mobi etc. formats, 

    .. code-block:: shell

        sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"


NES Game Emulator
-----------------

Emulators for games like :download:`Mario and Tank <download/NES-games.zip>`, 

    .. code-block:: shell

        sudo yum install gfceu



Vivado setup
------------

.. note:: 

    Login with 'Gnome on Xorg' otherwise following error will be shown, 
    
    .. code-block:: shell
    
        sudo ./xsetup
        ERROR: Installer could not be started. Could not initialize class sun.awt.X11GraphicsEnvironment

.. code-block:: shell

    sudo ./xsetup

    (add path to .bashrc)
    export PATH="/opt/Xilinx/Vivado/2017.4/bin:$PATH"

    (if required, change permission for .Xilinx folder)
    sudo chmod -R 777 .Xilinx/

    (run vivado to start)
    vivado